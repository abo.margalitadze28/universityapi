from sqlalchemy import ForeignKey
from extension import db


class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1000))
    surname = db.Column(db.String(1000))
    email = db.Column(db.String(100), unique=True)
    birthday = db.Column(db.String(1000))


class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1000))
    lecture_id = db.Column(db.Integer, ForeignKey('lecture.id'))


class StudentCourse(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    student_id = db.Column(db.Integer, ForeignKey('student.id'))
    course_id = db.Column(db.Integer, ForeignKey('course.id'))


class Lecture(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1000))
    surname = db.Column(db.String(1000))
    lecture_courses = db.relationship('Course', backref='lecture', lazy=True)


class StudentMark(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    exam_type = db.Column(db.String(30))
    exam_value = db.Column(db.Integer)
    course_id = db.Column(db.Integer, ForeignKey('course.id'))
    student_id = db.Column(db.Integer, ForeignKey('student.id'))
    create_time = db.Column(db.Time)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1000))
    surname = db.Column(db.String(1000))
    email = db.Column(db.String(100), unique=True)
    birthday = db.Column(db.String(1000))
    password=db.Column(db.String(1000))
    create_time=db.Column(db.Time)