from flask_login import LoginManager

# from application.student.main import student as student_blueprint
# from application.lecture.lecture import lecture
# from application.course.course import course
# from application.studentcourse.studentcourse import studentcourse
# from application.student_marks.studentmarks import studentmarks
# from application.registration.register import register
# from application.login.login import loggin
from extension import db, jwt
from flaskapp import app


def create_app():
    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    with app.app_context():
        db.create_all()

    app.register_blueprint(student_blueprint)
    app.register_blueprint(lecture)
    app.register_blueprint(course)
    app.register_blueprint(studentcourse)
    app.register_blueprint(studentmarks)
    app.register_blueprint(register)
    app.register_blueprint(loggin)

    jwt.init_app(app)
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(port=5001)



