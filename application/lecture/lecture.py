import json

from flask import Blueprint
from flask import request

from Models import Lecture
from application.lecture.conventers_LECT import Lecture_to_json
from application.login.login import login_required
from extension import db

lecture = Blueprint('lecture', __name__)


@lecture.route('/Lecture', methods=['POST'])
@login_required()

def add_lecture():
    name = request.json['name']
    surname = request.json['surname']

    new_lecture = Lecture(name=name, surname=surname)

    result = {
        "id": new_lecture.id,
        "name": name,
        "surname": surname,
    }

    db.session.add(new_lecture)
    db.session.commit()

    return json.dumps(result)


@lecture.route('/Lecture', methods=['GET'])
@login_required()

def get_Lecture():
    all_Lecture = Lecture.query.all()
    result = list(map(Lecture_to_json, all_Lecture))

    return json.dumps(result)


@lecture.route('/Lecture/<id>', methods=['GET'])
@login_required()

def Lecture_id(id):
    lecture = Lecture.query.get(id)
    result = {
        "id": lecture.id,
        "name": lecture.name,
        "surname": lecture.surname,
    }

    return json.dumps(result)


@lecture.route('/Lecture/<id>', methods=['DELETE'])
@login_required()

def Lecture_delete(id):

    lecture = Lecture.query.get(id)

    db.session.delete(lecture)
    db.session.commit()

    return "lecture delete in database"


@lecture.route('/Lecture/<id>', methods=['PUT'])
@login_required()

def change_Lecture(id):
    lecture = Lecture.query.get(id)

    name = request.json['name']
    surname = request.json['surname']

    lecture.name=name
    lecture.surname = surname
    db.session.commit()

    result = {
        "id": id,
        "name": name,
        "surname": surname,
    }

    return json.dumps(result)