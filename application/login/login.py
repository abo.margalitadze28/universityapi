import json

from flask import Blueprint
from flask import make_response
from flask import request
from datetime import datetime
from flask_jwt_extended import (
jwt_refresh_token_required,
get_jwt_identity,
create_refresh_token,
create_access_token,
    jwt_required
)
from functools import wraps

from Models import User
from extension import db

loggin = Blueprint('login', __name__)


def login_required():
    def wrapper(f):
        @wraps(f)
        @jwt_required
        def wrapped(*args, **kwargs):
            username = get_jwt_identity()
            if User.query.filter_by(name=username).first():
                return f(*args, **kwargs)
            return {'message': 'Please log in to continue'}, 401

        return wrapped

    return wrapper


def login_user(username):
    access_token = create_access_token(identity=username)
    refresh_token = create_refresh_token(identity=username)
    return access_token, refresh_token


@loggin.route('/login', methods=['GET'])
def login():
    name = request.json['name']
    password = request.json['password']

    if not name or not password :
        return make_response('Could not verify  aaa ', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    user = User.query.filter_by(name=name).first()

    if not user:
        return make_response('Could not verify eee', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    if user and user.password == password:

        print("oeee iuzgar ")
        access_token, refresh_token = login_user(name)
        return {
            'message': 'User {} was logged in'.format(name),
            'access_token': access_token,
            'refresh_token': refresh_token
        }

    return make_response('Could not verify oooo  ', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})


@loggin.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def TokenRefresh():
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}

