import json
from datetime import datetime

from flask import Blueprint
from flask import request

from Models import StudentMark
from application.login.login import login_required
from application.studentcourse.converters import marks_to_json
from extension import db

studentmarks = Blueprint('studentmarks', __name__)


@studentmarks.route('/StudentMarks', methods=['POST'])
@login_required()

def add_StudentMarks():
    exam_type = request.json['examtype']
    exam_value = request.json['examvalue']

    course_id = request.json['course_id']
    student_id = request.json['student_id']
    time = datetime.now()
    new_studentmarks = StudentMark(exam_type=exam_type, exam_value=exam_value, course_id=course_id,
                                   student_id=student_id,
                                   create_time=time)

    db.session.add(new_studentmarks)
    db.session.commit()

    result = {
        "id": new_studentmarks.id,
        "examtype": exam_type,
        "examvalue": exam_value,
        "course_id": course_id,
        "student_id": student_id,
        "time": str(time)
    }
    return json.dumps(result)


@studentmarks.route('/StudentMarks/<id>', methods=['GET'])
@login_required()

def get_StudentMarks(id):
    student_marks = StudentMark.query.filter_by(student_id=id)

    result = list(map(marks_to_json, student_marks))

    return json.dumps(result)
