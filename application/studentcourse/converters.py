import time

def marks_to_json(StudentMark):
    return {
        "id": StudentMark.id,
        "exam_type": StudentMark.exam_type,
        "exam_value":StudentMark. exam_value,
        "course_id": StudentMark. course_id,
        "student_id" : StudentMark.student_id,
        "create_time":str(StudentMark.create_time)
    }