import json

from flask import Blueprint
from flask import request

from Models import StudentCourse
from application.login.login import login_required
from extension import db

studentcourse = Blueprint('studentcourse', __name__)


@studentcourse.route('/Studentcourse', methods=['POST'])
@login_required()

def add_course():
    student_id = request.json['student_id']
    course_id = request.json['course_id']

    new_Studentcourse = StudentCourse(student_id=student_id, course_id=course_id)

    result = {
        "id": new_Studentcourse.id,
        "student_id": student_id,
        "course_id": course_id,
    }

    db.session.add(new_Studentcourse)
    db.session.commit()

    return json.dumps(result)