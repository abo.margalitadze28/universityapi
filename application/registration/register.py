import json

from flask import Blueprint
from flask import request
from datetime import datetime

from Models import User
from extension import db

register = Blueprint('register', __name__)


@register.route('/Registratin', methods=['POST'])
def add_user():
    name = request.json['name']
    surname = request.json['surname']

    email = request.json['email']
    birthday = request.json['birthday']
    password = request.json['password']
    time = datetime.now()
    role_name=request.json['role_name']
    new_user = User(name=name, surname=surname,email=email, birthday=birthday, password=password,
                    create_time=time,role_name=role_name)


    result = {
        "id": new_user.id,
        "name": name,
        "surname": surname,
        "email": email,
        "birthday": birthday,
        "password": password,
        "time": str(time)
    }

    db.session.add(new_user)
    db.session.commit()

    return json.dumps(result)