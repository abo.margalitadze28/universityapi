import json

from flask import Blueprint
from flask import request
from rest_framework import status

from Models import  Course
from application.course.convernters_course import Course_to_json
from application.login.login import login_required

from extension import db

course = Blueprint('course', __name__)


@course.route('/Course', methods=['POST'])
@login_required()

def add_course():
    name = request.json['name']
    lecture_id = request.json['lecture_id']

    new_course = Course(name=name, lecture_id=lecture_id)

    result = {
        "id": new_course.id,
        "name": name,
        "lecture": lecture_id,
    }

    db.session.add(new_course)
    db.session.commit()

    return json.dumps(result)

@course.route('/Course', methods=['GET'])
@login_required()

def get_Course():

    all_course = Course.query.all()
    result = list(map(Course_to_json, all_course))

    return json.dumps(result)

@course.route('/Course/<id>', methods=['GET'])
@login_required()

def gets_Course_id(id):
 try:
    course_id = Course.query.get(id)
    if course_id is None :
        raise KeyError('course id is not find')

    result = {
        "id": course_id.id,
        "name": course_id.name,
        "lecture_id": course_id.lecture_id,
    }

    return json.dumps(result)

 except KeyError as error:
     print(error)
     return {
                "error": repr(error)
            }, status.HTTP_400_BAD_REQUEST

 except Exception as error:
     print(error)
     return {
                "error": repr(error)
            },status.HTTP_500_INTERNAL_SERVER_ERROR


@course.route('/Course/<id>', methods=['PUT'])
@login_required()

def change_Course_id(id):

    course_id = Course.query.get(id)

    name = request.json['name']
    lecture_id = request.json['lecture_id']

    course_id.name=name
    course_id.lecture_id=lecture_id
    db.session.commit()

    result = {
        "id": id,
        "name": name,
        "lecture_id": lecture_id,
    }

    return json.dumps(result)


@course.route('/Course/<id>/lecture', methods=['PUT'])
@login_required()

def change_Course_lecture(id):

    course_id = Course.query.get(id)

    lecture_id = request.json['lecture_id']

    course_id.lecture_id=lecture_id
    db.session.commit()

    result = {
        "id": id,
        "name":course_id. name,
        "lecture_id": lecture_id,
    }

    return json.dumps(result)