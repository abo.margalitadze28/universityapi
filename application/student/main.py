import json

from flask import Blueprint
from flask import request
from rest_framework import status

from Models import Student, StudentCourse, Course, Lecture, StudentMark
from application.course.convernters_course import Course_to_json
from application.lecture.conventers_LECT import Lecture_to_json
from application.login.login import login_required
from application.student.converters_STUD import Student_to_json
from extension import db

student = Blueprint('student', __name__)


@student.route('/Student', methods=['POST'])
@login_required()

def add_Student():
    try:
        name = request.json['name']
        surname = request.json['surname']
        email = request.json['email']
        birthday = request.json['birthday']

        new_Student = Student(name=name, surname=surname, email=email, birthday=birthday)

        raise KeyError('email alredy exist')

        result = {
            "id": new_Student.id,
            "name": name,
            "surname": surname,
            "email": email,
            "birthday": birthday
        }

        db.session.add(new_Student)
        db.session.commit()
        return json.dumps(result)

    except KeyError as error:
        print(error)
        return {
                   "error": repr(error) + "is invalid"
               }, status.HTTP_400_BAD_REQUEST

    except Exception as error:
        print(error)
        return {
                   "error": repr(error)
               }, status.HTTP_500_INTERNAL_SERVER_ERROR


@student.route('/Student', methods=['GET'])
@login_required()

def get_Student():
    all_Student = Student.query.all()
    result = list(map(Student_to_json, all_Student))

    return json.dumps(result)


@student.route('/Student/<id>', methods=['GET'])
@login_required()

def Student_id(id):
    student = Student.query.get(id)

    result = {
        "id": student.id,
        "name": student.name,
        "surname": student.surname,
        "email": student.email,
        "birthday": student.birthday
    }

    return json.dumps(result)


@student.route('/Student/<id>', methods=['DELETE'])
@login_required()

def delete_student(id):
    student = Student.query.get(id)

    db.session.delete(student)
    db.session.commit()

    return "student delete in database"


@student.route('/Student/<id>', methods=['PUT'])
@login_required()

def change_Student(id):
    student = Student.query.get(id)

    name = request.json['name']
    surname = request.json['surname']
    email = request.json['email']
    birthday = request.json['birthday']

    student.name = name
    student.surname = surname
    student.email = email
    student.birthday = birthday
    db.session.commit()

    result = {
        "id": id,
        "name": name,
        "surname": surname,
        "email": email,
        "birthday": birthday
    }

    return json.dumps(result)


@student.route('/Student/<id>/lectore', methods=['GET'])
@login_required()

def get_Student_lectore(id):
    student_courses = StudentCourse.query.filter_by(student_id=id)
    lectore = []
    lectore_informatin = []
    for cur in student_courses:
        course = Course.query.filter_by(id=cur.course_id).first()
        lectore.append(course.lecture_id)
    for lect in lectore:
        lectore_info = Lecture.query.filter_by(id=lect).first()
        lectore_informatin.append(lectore_info)

    result = list(map(Lecture_to_json, lectore_informatin))

    return json.dumps(result)


@student.route('/Student/<id>/course', methods=['GET'])
@login_required()

def get_Student_course(id):
    student_courses = StudentCourse.query.filter_by(student_id=id)
    courses = []

    for cur in student_courses:
        course = Course.query.filter_by(id=cur.course_id).first()
        courses.append(course)

    result = list(map(Course_to_json, courses))

    return json.dumps(result)


@student.route('/Student/<id>/GPA', methods=['GET'])
@login_required()

def get_Student_GPA(id):
    allmarks = 0
    student_marks = StudentMark.query.filter_by(student_id=id)
    for marks in student_marks:
        allmarks += int(marks.exam_value)
    GPA = allmarks / student_marks.count()

    return json.dumps(GPA)


@student.route('/Student/search', methods=['GET'])
@login_required()

def get_student_search():
    try:
        name = request.args.get('name')
        surname = request.args.get('surname')
        email = request.args.get('email')
        if name is None and surname is None and email is None :
            raise KeyError('query parametrs is not true')

        size = 2
        index = 0

        search_name = "%{}%".format(name)
        search_surname = "%{}%".format(surname)
        search_email = "%{}%".format(email)

        if name is not None and surname is None and email is None:
            search = Student.query.filter(Student.name.like(search_name)).limit(size).offset(
                size * index).all()
            result = list(map(Student_to_json, search))
            return json.dumps(result)

        if name is not None and surname is not None and email is None:
            search = Student.query.filter(Student.name.like(search_name), Student.surname.like(search_surname)).limit(
                size).offset(
                size * index).all()
            result = list(map(Student_to_json, search))
            return json.dumps(result)

        if name is not None and surname is not None and email is not None:
            search = Student.query.filter(
                Student.name.like(search_name), Student.surname.like(search_surname), Student.email.like(
                    search_email)).limit(size).offset(
                size * index).all()
            result = list(map(Student_to_json, search))
            return json.dumps(result)

        if name is None and surname is not None and email is None:
            search = Student.query.filter(Student.surname.like(search_surname)).limit(size).offset(
                size * index).all()
            result = list(map(Student_to_json, search))
            return json.dumps(result)

        if name is None and surname is not None and email is not None:
            search = Student.query.filter(Student.surname.like(search_surname), Student.email.like(search_email)).limit(
                size).offset(
                size * index).all()
            result = list(map(Student_to_json, search))
            return json.dumps(result)

        if name is not None and surname is not None and email is None:
            search = Student.query.filter(Student.surname.like(search_surname),
                                          Student.surname.like(search_surname)).limit(
                size).offset(
                size * index).all()
            result = list(map(Student_to_json, search))
            return json.dumps(result)

        if name is None and surname is None and email is not None:
            search = Student.query.filter(Student.email.like(search_email)).limit(
                size).offset(
                size * index).all()
            result = list(map(Student_to_json, search))
            return json.dumps(result)
    except KeyError as error:
        print(error)
        return {
                   "error": repr(error) + "is invalid"
               }, status.HTTP_400_BAD_REQUEST


    except Exception as error:
          print(error)
          return {
              "error": repr(error)
              }, status.HTTP_500_INTERNAL_SERVER_ERROR
