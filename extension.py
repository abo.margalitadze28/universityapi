from flask_migrate import Migrate

from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

from flaskapp import app

db = SQLAlchemy()

migrate = Migrate(app, db)

jwt = JWTManager()