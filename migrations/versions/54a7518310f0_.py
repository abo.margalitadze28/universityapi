"""empty message

Revision ID: 54a7518310f0
Revises: 
Create Date: 2020-01-27 16:42:38.472920

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '54a7518310f0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('student')
    op.drop_table('lecture')
    op.drop_table('student_course')
    op.drop_table('course')
    op.drop_table('student_mark')
    op.drop_table('user')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('name', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('surname', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('email', sa.VARCHAR(length=100), autoincrement=False, nullable=True),
    sa.Column('birthday', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('password', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('create_time', postgresql.TIME(), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('id', name=u'user_pkey'),
    sa.UniqueConstraint('email', name=u'user_email_key')
    )
    op.create_table('student_mark',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('exam_type', sa.VARCHAR(length=30), autoincrement=False, nullable=True),
    sa.Column('exam_value', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('course_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('student_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('create_time', postgresql.TIME(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['course_id'], [u'course.id'], name=u'student_mark_course_id_fkey'),
    sa.ForeignKeyConstraint(['student_id'], [u'student.id'], name=u'student_mark_student_id_fkey'),
    sa.PrimaryKeyConstraint('id', name=u'student_mark_pkey')
    )
    op.create_table('course',
    sa.Column('id', sa.INTEGER(), server_default=sa.text(u"nextval('course_id_seq'::regclass)"), autoincrement=True, nullable=False),
    sa.Column('name', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('lecture_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['lecture_id'], [u'lecture.id'], name=u'course_lecture_id_fkey'),
    sa.PrimaryKeyConstraint('id', name=u'course_pkey'),
    postgresql_ignore_search_path=False
    )
    op.create_table('student_course',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('student_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('course_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['course_id'], [u'course.id'], name=u'student_course_course_id_fkey'),
    sa.ForeignKeyConstraint(['student_id'], [u'student.id'], name=u'student_course_student_id_fkey'),
    sa.PrimaryKeyConstraint('id', name=u'student_course_pkey')
    )
    op.create_table('lecture',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('name', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('surname', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('id', name=u'lecture_pkey')
    )
    op.create_table('student',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('name', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('surname', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.Column('email', sa.VARCHAR(length=100), autoincrement=False, nullable=True),
    sa.Column('birthday', sa.VARCHAR(length=1000), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('id', name=u'student_pkey'),
    sa.UniqueConstraint('email', name=u'student_email_key')
    )
    # ### end Alembic commands ###
